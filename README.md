## Overview

This script is used to rename `master` to `main` in a given GitLab project

**NOTE** This does not adjust anything in your `.gitlab-ci.yml`, that will still need to be done manually

## Detailed Actions

* Creates a new branch based on the legacy name

* Sets the default branch to the branch created above

* Deletes the old branch

* Sets a new protected branch with the old name, with push and merge denied (preventing accidental recreations)

## Usage

Install requirements with:

```
pip3 install -r ./requirements.txt
```

Get a GitLab API token using the instructions
[here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token).
Grant the token all API access.

Run Using:

```
./fix.py --gitlab-token TOKEN_FROM_ABOVE PROJECT_ID
```

Full Help

```
./fix.py -h                                                                                                                                                            main
usage: fix.py [-h] [-v] [-g GITLAB_SERVER] [-t GITLAB_TOKEN] [-l LEGACY_TERM] [-n NEW_TERM] project_id

This is my super sweet script

positional arguments:
  project_id            Project ID to fix. You can find this on the main page of your GitLab project

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Be verbose
  -g GITLAB_SERVER, --gitlab-server GITLAB_SERVER
                        Gitlab server to connect to (Default: https://gitlab.oit.duke.edu)
  -t GITLAB_TOKEN, --gitlab-token GITLAB_TOKEN
                        Gitlab Token to use (Default: GITLAB_TOKEN env variable)
  -l LEGACY_TERM, --legacy-term LEGACY_TERM
                        Term to move away from (default: master)
  -n NEW_TERM, --new-term NEW_TERM
                        New term to use (default: main)
```
