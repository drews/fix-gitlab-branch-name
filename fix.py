#!/usr/bin/env python3
import sys
import argparse
import logging
import os
import gitlab


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description="This is my super sweet script")
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )
    parser.add_argument(
        "-g",
        "--gitlab-server",
        help="Gitlab server to connect to (Default: https://gitlab.oit.duke.edu)",
        default=os.environ.get("GITLAB_SERVER", "https://gitlab.oit.duke.edu"),
    )
    parser.add_argument(
        "-t",
        "--gitlab-token",
        help="Gitlab Token to use (Default: GITLAB_TOKEN env variable)",
        default=os.environ.get("GITLAB_TOKEN"),
    )
    parser.add_argument(
        "-l",
        "--legacy-term",
        default="master",
        type=str,
        help="Term to move away from (default: master)",
    )
    parser.add_argument(
        "-n",
        "--new-term",
        type=str,
        default="main",
        help="New term to use (default: main)",
    )

    parser.add_argument(
        "project_id",
        type=int,
        help="Project ID to fix.  You can find this on the main page of your GitLab project",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    if args.legacy_term == args.new_term:
        sys.stderr.write("❗️ new-term and legacy-term must be different\n")
        return 1

    gl = gitlab.Gitlab(args.gitlab_server, private_token=args.gitlab_token)
    gl.auth()

    project = gl.projects.get(args.project_id)
    print(f"Project: {project.name}")
    existing_branch_names = []
    for existing_branch in project.branches.list():
        existing_branch_names.append(existing_branch.name)

    if args.legacy_term not in existing_branch_names:
        logging.info(f"✅ Great, {args.legacy_term} is not in use!")
        return 0

    if args.new_term not in existing_branch_names:
        logging.info(f"Cloning {args.legacy_term} to {args.new_term}")
        project.branches.create({"branch": args.new_term, "ref": args.legacy_term})

    if project.default_branch != args.new_term:
        logging.info(f"Setting default branch to {args.new_term}")
        project.default_branch = args.new_term
        project.save()

    logging.info(f"Deleting old branch: {args.legacy_term}")
    project.branches.delete(args.legacy_term)

    for item in project.protectedbranches.list():
        if item.name == args.legacy_term:
            logging.info(f"Deleting old protection for {args.legacy_term}")
            item.delete()

    logging.info(f"Protecting {args.legacy_term} to prevent it from being added back")
    project.protectedbranches.create(
        {"name": args.legacy_term, "merge_access_level": 0, "push_access_level": 0,}
    )

    return 0


if __name__ == "__main__":
    sys.exit(main())
